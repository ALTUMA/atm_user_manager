<?php

namespace ATM\UserManagerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use ATM\UserManagerBundle\Entity\UserManagerFields;

class UserManagerFieldsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('disabled_description',TextareaType::class,array(
                'required' => false,
                'label' => 'disabled description'
            ))
            ->add('disabled_at',DateType::class,array(
                'required' => false,
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => UserManagerFields::class
        ));
    }

    public function getBlockPrefix()
    {
        return 'atm_user_management_fields';
    }
}