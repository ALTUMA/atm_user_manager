<?php

namespace ATM\UserManagerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('username',TextType::class,array(
            //    'required' => false
            //))
            //->add('email',EmailType::class,array(
            //    'required' => false
            //))
            ->add('enabled',CheckboxType::class,array(
                'required' => false
            ))
            ->add('locked',CheckboxType::class,array(
                'required' => false
            ))
            ->add('expired',CheckboxType::class,array(
                'required' => false
            ))
            ->add('expiresAt',DateType::class,array(
                'required' => false,
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy'
            ))
            ->add('userManagerFields', UserManagerFieldsType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

    public function getBlockPrefix()
    {
        return 'atm_user_management_bundle';
    }
}