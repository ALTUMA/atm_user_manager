<?php

namespace ATM\UserManagerBundle\Controller;

use ATM\UserManagerBundle\Entity\UserManagerFields;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\RedirectResponse;
use ATM\UserManagerBundle\Form\UserType;

class ManagerController extends Controller
{
    /**
     * @Route("/",name="atm_user_manager_index")
     */
    public function indexAction()
    {
        $session = $this->get('session');
        $searchWord = null;
        if($session->has('searchWord')){
            $searchWord = $session->get('searchWord');
            $session->remove('searchWord');
        }

        return $this->render('ATMUserManagerBundle:Manager:index.html.twig',array(
            'searchWord' => $searchWord
        ));
    }

    /**
     * @Route("/search/user/{searchText}",name="search_users", options={"expose"=true})
     */
    public function searchUsersAction($searchText){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_user_manager');

        $qb = $em->createQueryBuilder();

        $qb
            ->select('partial u.{id,username,email,joindate,enabled,expired,locked}')
            ->from($config['user'],'u')
            ->orderBy('u.username');

        if(filter_var($searchText, FILTER_VALIDATE_EMAIL) !== FALSE){
            $qb->where($qb->expr()->eq('u.email',$qb->expr()->literal($searchText)));
        }else{
            $qb->where(
                $qb->expr()->orX(
                    $qb->expr()->like('u.username',$qb->expr()->literal('%'.$searchText.'%')),
                    $qb->expr()->like('u.email',$qb->expr()->literal('%'.$searchText.'%'))
                )
            );
        }

        $users = $qb->getQuery()->getArrayResult();

        return $this->render('ATMUserManagerBundle:Manager:searchResults.html.twig',array(
            'users' => $users
        ));
    }

    /**
     * @Route("/edit/user/{userId}",name="edit_user", options={"expose"=true})
     */
    public function editUserAction($userId){
        $em = $this->getDoctrine()->getManager();
        $config = $this->getParameter('atm_user_manager');
        $request = $this->get('request_stack')->getCurrentRequest();

        $searchWord = $request->get('searchWord');

        $user = $em->getRepository($config['user'])->findOneById($userId);
        $form = $this->createForm(UserType::class,$user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $em->persist($user);
            $em->flush();

            $userManagerFields = $user->getUserManagerFields();

            if(is_null($userManagerFields)){
                $userManagerFields = new UserManagerFields();
                $userManagerFields->setUser($user);
                $em->persist($userManagerFields);
                $em->flush();
            }

            if($user->isEnabled()){
                if(!is_null($userManagerFields->getDisabledDescription())){
                    $userManagerFields->setDisabledDescription(null);
                }

                if(!is_null($userManagerFields->getDisabledAt())){
                    $userManagerFields->setDisabledAt(null);
                }
            }

            if(is_null($userManagerFields->getUser())){
                $userManagerFields->setUser($user);
            }
            $em->persist($userManagerFields);
            $this->get('session')->set('searchWord',$searchWord);

            $em->persist($user);
            $em->flush();

            return new RedirectResponse($this->get('router')->generate('atm_user_manager_index'));
        }

        return $this->render('ATMUserManagerBundle:Manager:editUserForm.html.twig',array(
            'user' => $user,
            'form' => $form->createView(),
            'searchWord' => $searchWord
        ));
    }
}

