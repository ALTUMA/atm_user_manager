<?php

namespace ATM\UserManagerBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user_manager_fields")
 */
class UserManagerFields{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="blocked_description", type="text", nullable=true)
     */
    private $disabled_description;

    /**
     * @ORM\Column(name="disabled_at", type="datetime", nullable=true)
     */
    private $disabled_at;


    protected $user;


    public function getId()
    {
        return $this->id;
    }

    public function getDisabledDescription()
    {
        return $this->disabled_description;
    }

    public function setDisabledDescription($disabled_description)
    {
        $this->disabled_description = $disabled_description;
    }

    public function getDisabledAt()
    {
        return $this->disabled_at;
    }

    public function setDisabledAt($disabled_at)
    {
        $this->disabled_at = $disabled_at;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }
}